import {Component, OnInit} from '@angular/core';
import {Empleado} from '../../_class/empleado';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  public name;
  private age = 5678;
  public trabajos: Array<string> = ['ing', 'jefe'];
  public empleado: Empleado;
  public trabajadores: Array<Empleado>;
  public trabajadorExterno: boolean;
  public color: string;

  constructor() {
    this.trabajadorExterno = false;
    this.color = 'blue';
    this.name = 'Julian Jimenez';
    this.empleado = new Empleado('julian', 34, 'ing', true);
    this.trabajadores = [new Empleado('camillo', 27, 'agile', true),
      new Empleado('alberto', 15, 'ing', true),
      new Empleado('pablo', 16, 'ing', false)];
  }

  ngOnInit() {
  }


  cambiarExterno() {
    this.trabajadorExterno = !this.trabajadorExterno;
  }

}
